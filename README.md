# Java Specs

Proyecto para manejar los planes en bamboo

## Getting Started

Para correr el proyecto es necesario revisar ciertas cosas. A continuación las describo

### Prerequisites

Lo primero será revisar el pom.xml y confirmar que la versión de bamboo sea la misma

```
  <parent>
    <groupId>com.atlassian.bamboo</groupId>
    <artifactId>bamboo-specs-parent</artifactId>
    <version>7.0.3</version>
    <relativePath/>
  </parent>
```

Lo segundo será agregar en .credentials tu usuario y contraseña para que se pueda conectar a tu instancia de bamboo

```
username=
password=
```


Por último solo basta irte a config.properties y modificar las variables necesarias.
```
    bambooServer=http://10.12.0.162:8085/
    repositoryPlantillasDespliegue=
    applicationLinkName=
```

### Corriendo bamboo Specs

Para correr el proyecto es necesario este comando:


```
mvn -Ppublish-specs
```

### Probando bamboo Specs

El proyecto cuenta con pruebas unitarias. Recomiendo hacer al menos 1 prueba por template.

```
mvn test
```


### Generar un proyecto de java specs desde 0

Si deseas crear un proyecto de java compatible con bamboo puedes correr este comando para tener las bases:


```
mvn archetype:generate "-B" "-DarchetypeGroupId=com.atlassian.bamboo" "-DarchetypeArtifactId=bamboo-specs-archetype" "-DarchetypeVersion=6.2.1" "-DgroupId=com.my.company" "-DartifactId=bamboo-specs" "-Dversion=1.0.0-SNAPSHOT" "-Dpackage=com.my.company"
```

## Tecnologías Utilizadas

| Plugin | README |
| ------ | ------ |
| bamboo-specs-api | [https://docs.atlassian.com/bamboo-specs-docs/6.9.2/specs-java.html] |
| com.google.code.gson | [https://mvnrepository.com/artifact/com.google.code.gson/gson/2.8.0]|
| junit | [https://mvnrepository.com/artifact/junit/junit]|
