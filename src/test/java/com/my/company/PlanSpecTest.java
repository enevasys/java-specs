package com.my.company;

import com.atlassian.bamboo.specs.api.builders.Variable;
import com.my.company.templates.build.TemplateUno;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.my.company.templates.build.TemplateAws;
import com.my.company.templates.build.TemplateBaseBuild;
import com.my.company.templates.build.TemplateDos;
import com.my.company.templates.build.TemplateFront;
import com.my.company.templates.build.TemplateOnPremise;
import com.my.company.templates.deploy.TemplateBaseDeploy;
import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

public class PlanSpecTest {

    @Test
    public void checkBuildOffline() throws PropertiesValidationException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");
        Class<?> buildClass = Class.forName("com.my.company.templates.build.TemplateUno");
        TemplateBaseBuild template = (TemplateBaseBuild) buildClass.getDeclaredConstructor(TemplateBaseBuild.getConstructorClassArgs()).newInstance("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "MICROP1", "Micro servicio prueba 1", "prueba 1", "node-crud", variables, "master", "MSW");

        EntityPropertiesBuilders.build(template.getPlan());
    }

    @Test
    public void checkDeployOffline() throws PropertiesValidationException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");
        Class<?> deployClass = Class.forName("com.my.company.templates.deploy.TemplateOnPremise");

        TemplateBaseDeploy template = (TemplateBaseDeploy) deployClass.getDeclaredConstructor(TemplateBaseDeploy.getConstructorClassArgs()).newInstance("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "Micro servicio prueba 1", variables, "desarrollo");

        EntityPropertiesBuilders.build(template.createDeploymentObject());
    }

    @Test
    public void checkTemplateUno() throws PropertiesValidationException {

        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");

        TemplateUno plan = new TemplateUno("PROJID", "Nombre Proyecto", "descripcion", "PLANKEY", "Nombre plan", "desc Proyecto", "repo-slug", variables, "master", "MSW");

        EntityPropertiesBuilders.build(plan.getPlan());
    }

    @Test
    public void checkTemplateDos() throws PropertiesValidationException {

        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");

        TemplateDos plan = new TemplateDos("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "MICROP1", "Micro servicio prueba 1", "prueba 1", "node-crud", variables, "master", "MSW");

        EntityPropertiesBuilders.build(plan.getPlan());
    }

    @Test
    public void checkTemplateOnPremiseBuild() throws PropertiesValidationException {

        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");
        TemplateOnPremise plan = new TemplateOnPremise("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "MICROP1", "Micro servicio prueba 1", "prueba 1", "node-crud", variables, "master", "MSW");

        EntityPropertiesBuilders.build(plan.getPlan());
    }

    @Test
    public void checkTemplateOnPremiseDeploy() throws PropertiesValidationException {
        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");
        com.my.company.templates.deploy.TemplateOnPremise deploy = new com.my.company.templates.deploy.TemplateOnPremise("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "descripcion del deploy", variables, "desarrollo");
        EntityPropertiesBuilders.build(deploy.createDeploymentObject());
    }

    @Test
    public void checkTemplateFrontBuild() throws PropertiesValidationException {

        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");
        TemplateFront plan = new TemplateFront("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "MICROP1", "Micro servicio prueba 1", "prueba 1", "node-crud", variables, "master", "MSW");

        EntityPropertiesBuilders.build(plan.getPlan());
    }

    @Test
    public void checkTemplateFrontDeploy() throws PropertiesValidationException {
        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");
        com.my.company.templates.deploy.TemplateFront deploy = new com.my.company.templates.deploy.TemplateFront("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "descripcion del deploy", variables, "produccion");
        EntityPropertiesBuilders.build(deploy.createDeploymentObject());
    }

    @Test
    public void checkTemplateAwsBuild() throws PropertiesValidationException {

        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");
        TemplateAws plan = new TemplateAws("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "MICROP1", "Micro servicio prueba 1", "prueba 1", "node-crud", variables, "master", "MSW");

        EntityPropertiesBuilders.build(plan.getPlan());
    }

    @Test
    public void checkTemplateAwsDeploy() throws PropertiesValidationException {
        Variable[] variables = new Variable[1];
        variables[0] = new Variable("prueba", "prueba");
        com.my.company.templates.deploy.TemplateAws deploy = new com.my.company.templates.deploy.TemplateAws("PRUEBA1", "PRUEBA1", "Proyecto-prueba", "descripcion del deploy", variables, "none");
        EntityPropertiesBuilders.build(deploy.createDeploymentObject());
    }
}
