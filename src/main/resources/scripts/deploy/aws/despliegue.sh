echo 'Obteniendo prefijo y profile'
set -e
prefijo=''
profile=''
if [ -f "config-microservicios/QA/Config_Microservicios.json" ];
    then
        prefijo=$(jq ".${bamboo.ambiente}.${bamboo.variante}.stage" config-microservicios/QA/Config_Microservicios.json)
        profile=$(jq ".${bamboo.ambiente}.${bamboo.variante}.profile" config-microservicios/QA/Config_Microservicios.json)
        echo $prefijo
        echo $profile
    else
        echo "No hay archivo de config"

fi
if [ -f "layer/nodejs/package.json" ];
    then
        cd layer/nodejs/
        npm install
    else
        echo "No hay archivo layer/nodejs/package.json"

fi
npm install
serverless deploy --aws-profile ${profile} --stage ${prefijo}