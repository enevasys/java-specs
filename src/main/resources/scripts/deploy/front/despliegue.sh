#!/bin/bash
echo "trabajando con los siguientes archivos"
set -e
dir
echo 'Despliegue Kube'
cd YML
dir
cd ..
cd YML
dir
cd ..

echo 'Obteniendo prefijo'
prefijo=''
prefijo=$(jq '.${bamboo.ambiente}.stage' config-microservicios/QA/Config_Microservicios.json)
echo $prefijo

echo 'Obteniendo variables para guardar la imagen'
deployment_file=$(ls YML/*deployment.yml | head -n 1)
echo $deployment_file
imagen=''
namespace=''
if [ "${deployment_file}" != "" ] && [ -f ${deployment_file} ];
    then
		imagen=$(yq r $deployment_file "metadata.labels.app")
        registry=$(jq -r ".${bamboo.ambiente}.registry" config-microservicios/QA/Config_Microservicios.json)
        echo "registry:"
        echo $registry
        sed -i 's/desarrollo/'"${bamboo.ambiente}"'/g' "${deployment_file}"
        sed -i "s|hub.donde.org.mx|${registry//\"/}/${prefijo//\"/}|g" ${deployment_file}
        sed -i "s|localhost:5000|${registry//\"/}/${prefijo//\"/}|g" "${deployment_file}"
        echo "pull policy"
        sed -i 's/#imagePullPolicy/imagePullPolicy/g' "${deployment_file}"
        tipo=$(yq r $deployment_file "kind")
        nombreYML=$(yq r $deployment_file "metadata.name")
        namespace=$(yq r $deployment_file "metadata.namespace")
        echo $tipo
        echo $nombreYML
        echo $namespace
		if (kubectl get "$tipo" -n "$namespace" --kubeconfig=$HOME/.kube/config_${prefijo//\"/} |grep ^"${nombreYML}");
            then
	            kubectl delete -f "${deployment_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}

        fi
        kubectl create -f "${deployment_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
        
    else
        echo "No, no existen los YML"
fi

services_file=$(ls YML/*services.yml | head -n 1)
echo $services_file
if [ "${services_file}" != "" ] && [ -f ${services_file} ];
    then
        #set -o posix ; set
        tipo=$(yq r $services_file "kind")
        nombreYML=$(yq r $services_file "metadata.name")
        namespace=$(yq r $services_file "metadata.namespace")
        echo $tipo
        echo $nombreYML
        echo $namespace
		if (kubectl get "$tipo" -n "$namespace" --kubeconfig=$HOME/.kube/config_${prefijo//\"/} |grep ^"${nombreYML}");
            then
	            kubectl delete -f "${services_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
        fi
        kubectl create -f "${services_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
fi
x=''
ingress_file=$(ls YML/*ingress.yml | head -n 1)
echo $ingress_file
if [ "${ingress_file}" != "" ] && [ -f ${ingress_file} ];
    then
        tipo=$(yq r $ingress_file "kind")
        nombreYML=$(yq r $ingress_file "metadata.name")
        namespace=$(yq r $ingress_file "metadata.namespace")
        echo $tipo
        echo $nombreYML
        echo $namespace
        yq w -i $ingress_file "metadata.annotations.[kubernetes.io/ingress.class]" nginx
		yq d -i $ingress_file "metadata.annotations.[traefik.ingress.kubernetes.io/affinity]"
        DNS=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".DNS" config-microservicios/QA/Config_Microservicios.json)
        if [ "$DNS" != "" ];
            then
                echo "dns no es null"
                for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".DNS | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
                    value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".DNS[$key]" config-microservicios/QA/Config_Microservicios.json);                    
                    #sed -i 's/${txt[0]}/${txt[1]}/g' ${ingress}"
                    sed -i "s|${key//\"/}|""${value}""|g" "${ingress_file}"
                done
                echo "archivo ingress"
                cat ${ingress_file}
        fi
		if (kubectl get "$tipo" -n "$namespace" --kubeconfig=$HOME/.kube/config_${prefijo//\"/} |grep ^"${nombreYML}");
            then
	            kubectl delete -f "${ingress_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
        fi
        kubectl create -f "${ingress_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
fi

echo "rollout"
#kubectl rollout status deployment/${nombre_contenedor}-deployment -n ${metadata_namespace}
echo "kubectl rollout status deployment/${imagen}-deployment -n ${namespace} --kubeconfig=$HOME/.kube/config_${prefijo//\"/}"
kubectl rollout status deployment/${imagen}-deployment -n ${namespace} --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
echo $?
