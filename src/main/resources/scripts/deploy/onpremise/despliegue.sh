#!/bin/bash
set -e
echo "trabajando con los siguientes archivos"
dir
echo 'Despliegue Kube'
cd YML
dir
cd ..
cd YML
dir
cd ..

echo 'Obteniendo prefijo'
prefijo=''
prefijo=$(jq '.${bamboo.ambiente}.stage' config-microservicios/QA/Config_Microservicios.json)
echo $prefijo


echo 'Obteniendo variables para guardar la imagen'

deployment_file=$(ls YML/*deployment.yml | head -n 1)
echo $deployment_file
imagen=''
namespace=''
if [ "${deployment_file}" != "" ] && [ -f ${deployment_file} ];
    then
		imagen=$(yq r $deployment_file "metadata.labels.app")
        sed -i 's/desarrollo/'"${bamboo.ambiente}"'/g' "${deployment_file}"
        registry=$(jq -r ".${bamboo.ambiente}.registry" config-microservicios/QA/Config_Microservicios.json)
        echo "registry:"
        echo $registry
        sed -i "s|hub.donde.org.mx|${registry//\"/}/${prefijo//\"/}|g" ${deployment_file}
        sed -i "s|localhost:5000|${registry//\"/}/${prefijo//\"/}|g" "${deployment_file}"
        echo "pull policy"
        sed -i 's/#imagePullPolicy/imagePullPolicy/g' "${deployment_file}"
        
        tipo=$(yq r $deployment_file "kind")
        nombreYML=$(yq r $deployment_file "metadata.name")
        namespace=$(yq r $deployment_file "metadata.namespace")
        echo $tipo
        echo $nombreYML
        echo $namespace
		if (kubectl get "$tipo" -n "$namespace" --kubeconfig=$HOME/.kube/config_${prefijo//\"/} |grep ^"${nombreYML}");
            then
	            kubectl delete -f "${deployment_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}

        fi
        kubectl create -f "${deployment_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
        
    else
        echo "No, no existen los YML"
fi
autoscaler_file=$(ls YML/*autoscaler.yml | head -n 1)
echo $autoscaler_file
if [ "${autoscaler_file}" != "" ] && [ -f ${autoscaler_file} ];
    then
        tipo=$(yq r $autoscaler_file "kind")
        nombreYML=$(yq r $autoscaler_file "metadata.name")
        namespace=$(yq r $autoscaler_file "metadata.namespace")
        echo $tipo
        echo $nombreYML
        echo $namespace
		if (kubectl get "$tipo" -n "$namespace" --kubeconfig=$HOME/.kube/config_${prefijo//\"/} |grep ^"${nombreYML}");
            then
	            kubectl delete -f "${autoscaler_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
        fi
        kubectl create -f "${autoscaler_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
fi

services_file=$(ls YML/*services.yml | head -n 1)
echo $services_file
service_kong_port=""
service_kong=""
if [ "${services_file}" != "" ] && [ -f ${services_file} ];
    then
        #set -o posix ; set
        tipo=$(yq r $services_file "kind")
        nombreYML=$(yq r $services_file "metadata.name")
        namespace=$(yq r $services_file "metadata.namespace")
        service_kong_port=$(yq r $services_file "spec.ports[0].port")
        service_kong="${nombreYML}.${namespace}.svc.cluster.local"
        echo $tipo
        echo $nombreYML
        echo $namespace
		if (kubectl get "$tipo" -n "$namespace" --kubeconfig=$HOME/.kube/config_${prefijo//\"/} |grep ^"${nombreYML}");
            then
	            kubectl delete -f "${services_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
        fi
        kubectl create -f "${services_file}" --kubeconfig=$HOME/.kube/config_${prefijo//\"/}
fi
echo "probando variables kong"
echo $service_kong_port
echo $service_kong
x='
ingress_file=$(ls YML/*ingress.yml | head -n 1)
echo $ingress_file
if [ -f ${ingress_file} ];
    then
        tipo=$(yq r $ingress_file "kind")
        nombreYML=$(yq r $ingress_file "metadata.name")
        namespace=$(yq r $ingress_file "metadata.namespace")
        echo $tipo
        echo $nombreYML
        echo $namespace
		if (kubectl get "$tipo" -n "$namespace" |grep ^"${nombreYML}");
            then
	            kubectl delete -f "${ingress_file}"
        fi
        kubectl create -f "${ingress_file}"
fi
'
echo "rollout"
#kubectl rollout status deployment/${nombre_contenedor}-deployment -n ${metadata_namespace}
echo "kubectl rollout status deployment/${imagen}-deployment -n ${namespace} --kubeconfig=$HOME/.kube/config_${prefijo//\"/}"
kubectl rollout status deployment/${imagen}-deployment -n ${namespace} --kubeconfig=$HOME/.kube/config_${prefijo//\"/}

echo "kong"


#kong
kong_ip=$(jq -r ".${bamboo.ambiente}.kong_ip" config-microservicios/QA/Config_Microservicios.json);
echo $kong_ip

#kong_ip="172.16.3.54"

kong_ingress=$(jq -r ".${bamboo.ambiente}.kong_ingress" config-microservicios/QA/Config_Microservicios.json);
echo $kong_ingress

#kong_ingress="172.16.3.53"
api_kong=$(curl -i -k https://${kong_ip}:8444/services/api-${imagen} 2>/dev/null | head -n 1 | cut -d' ' -f2)
rt_kong=$(curl -i -k https://${kong_ip}:8444/routes/rt-${imagen} 2>/dev/null | head -n 1 | cut -d' ' -f2)
acl_kong=$(curl -i -k https://${kong_ip}:8444/services/api-${imagen}/plugins 2>/dev/null | head -n 1 | cut -d' ' -f2)
echo $api_kong
echo $rt_kong
echo $acl_kong
echo "dataCrearServicio"
if [ -f "YML/kong/dataCrearServicio.json" ];
    then
        #ejemplos jenkinsfile
        #sed -i "s/10.10.58.75/${kong_ingress}/g" YML/kong/dataCrearServicio.json
        #sh("jq '.port = ${service_kong_port} | .host=\"${service_kong}\"' YML/kong/dataCrearServicio.json > YML/kong/dataCrearServicio.json_2")
        #sh("mv YML/kong/dataCrearServicio.json_2 YML/kong/dataCrearServicio.json")
        jq ".port = ${service_kong_port} | .host=\"${service_kong}\"" YML/kong/dataCrearServicio.json  > YML/kong/dataCrearServicio.json_2
        mv YML/kong/dataCrearServicio.json_2 YML/kong/dataCrearServicio.json
        if [ $api_kong != '200' ];
            then
                #usar ip 55
                echo "no es 200"
                curl -d @./YML/kong/dataCrearServicio.json -H 'Content-Type: application/json' -X PUT -k https://${kong_ip}:8444/services/api-${imagen} 
            else
                echo "si es 200"
                curl -d @./YML/kong/dataCrearServicio.json -H 'Content-Type: application/json' -X PATCH -k https://${kong_ip}:8444/services/api-${imagen}
        fi
    else
        echo "No existe /YML/kong/dataCrearServicio.json"
fi
echo "dataCrearRuta"
if [ -f "YML/kong/dataCrearRuta.json" ];
    then
        if [ $rt_kong != '200' ];
            then
                curl -d @./YML/kong/dataCrearRuta.json -H 'Content-Type: application/json' -X POST -k https://${kong_ip}:8444/services/api-${imagen}/routes/  
            else
                curl -d @./YML/kong/dataCrearRuta.json -H 'Content-Type: application/json' -X PATCH -k https://${kong_ip}:8444/routes/rt-${imagen}
        fi
    else
        echo "No existe /YML/kong/dataCrearRuta.json"
fi

echo $imagen
echo "dataCrearACL"
if [ -f "YML/kong/dataCrearACL.json" ];
    then
        if [ $acl_kong != '200' ];
            then
                curl -d @./YML/kong/dataCrearACL.json -H 'Content-Type: application/json' -X POST -k https://${kong_ip}:8444/services/api-${imagen}/plugins
            else
                echo "entro al else"
                curl -k https://${kong_ip}:8444/services/api-${imagen}/plugins > acl-kong.json
                cat acl-kong.json
                estatus_json=$(jq -r ".data[].name" acl-kong.json);
                echo "Estatus json:"
                echo $estatus_json
                if [ "$estatus_json" != "[acl]" ];
                    then
                        curl -d @./YML/kong/dataCrearACL.json -H 'Content-Type: application/json' -X POST -k https://${kong_ip}:8444/services/api-${imagen}/plugins
                    else
                        echo "Existe plugin"
                        estatus_json=$(jq -r ".data[].id" acl-kong.json);
                        estatus_json=$estatus_json:1} 
                        estatus_json=${estatus_json//]}
                        echo $estatus_json
                        curl -d @./YML/kong/dataCrearACL.json -H 'Content-Type: application/json' -X PATCH -k https://${kong_ip}:8444/services/api-${imagen}/plugins/${estatus_json}
                fi
        fi
    else
        echo "No existe /YML/kong/dataCrearRuta.json"
fi


##################