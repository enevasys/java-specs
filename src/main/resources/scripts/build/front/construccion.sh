#!/bin/bash
set -e
dir
echo 'Obteniendo prefijo'
prefijo=''
prefijo=$(jq '.${bamboo.ambiente}.stage' config-microservicios/QA/Config_Microservicios.json)
echo $prefijo

registry=$(jq '.${bamboo.ambiente}.registry' config-microservicios/QA/Config_Microservicios.json)
echo "registry:"
echo $registry

echo 'Creando directorio imagen'
if [ -d YML/Docker/imagen ];
then
	rm -R YML/Docker/imagen
fi

mkdir YML/Docker/imagen
rsync -a ./ YML/Docker/imagen
mv YML/Docker/imagen/YML YML/Docker/
#./YML/Docker/YML/pv-wanashop-front-deployment.yml
#rm -f YML/pv-wanashop-front-deployment.yml
#rm -f YML/ppv-wanashop-front-services.yml
#rm -f YML/pv-wanashop-front-ingress.yml
rm -f YML/Docker/imagen/README.md
rm -rf YML/Docker/imagen/.git
x='
rsync -Rr * YML/Docker/imagen
#rm -rf YML/Docker/imagen/YML
rm -f YML/Docker/imagen/YML/pv-wanashop-front-deployment.yml
if [ -f YML/Docker/imagen/README.md ];
then
	rm -f YML/Docker/imagen/README.md
fi
'					
echo 'Obteniendo variables para guardar la imagen'
echo 'probando buscador de nombre'
deployment_file=$(ls YML/*deployment.yml | head -n 1)
echo $deployment_file

nombre_contenedor=""
tagImage=""
if [ "${deployment_file}" != "" ] && [ -f ${deployment_file} ];
    then
      spec_template_spec_containers_image=$(yq r $deployment_file "spec.template.spec.containers[0].image")
      tagImage=${spec_template_spec_containers_image##*:}
      nombre_contenedor=$(yq r $deployment_file "metadata.labels.app")

    else
      echo "No, no existen los YML"
      exit 1
fi
echo "variables"
echo $nombre_contenedor
echo $tagImage
cd YML/Docker
dir
#echo ${nombre_contenedor,,}
echo "Se sustituye la version del dockerfile"
sed -i 's/USER nginx/#USER nginx/g' Dockerfile
#donde
docker build -t ${nombre_contenedor}:${tagImage} .
#sudo docker build --rm --no-cache -t ${nombre_contenedor}-enevasys:${tagImage} .

echo 'Tag de Imagen'
echo ${nombre_contenedor}
docker tag ${nombre_contenedor}:${tagImage} ${registry//\"/}/${prefijo//\"/}/${nombre_contenedor}:${tagImage}

echo 'Push de Imagen'
echo "docker push ${registry//\"/}/${prefijo//\"/}/${nombre_contenedor}:${tagImage}"
docker push ${registry//\"/}/${prefijo//\"/}/${nombre_contenedor}:${tagImage}

cd ../..