#!/bin/bash
set -e
echo "cambio parametria"
decode () {
  echo "$1" | base64 -d ; echo
}
config=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".config" config-microservicios/QA/Config_Microservicios.json)
echo "config: ${config}"

microservicios=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".microservicios" config-microservicios/QA/Config_Microservicios.json)
if [ "$microservicios" != null ];
    then
        for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".microservicios | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
            value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".microservicios[$key]" config-microservicios/QA/Config_Microservicios.json);                    
            realkey=$(echo "${key//\"/}"  | sed  's/\\//')
            sed -i "s|${realkey//\"/}|""${value}""|g" .env.production
        done
fi
PROVISION_KEY_ID=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".PROVISION_KEY_ID" config-microservicios/QA/Config_Microservicios.json)
if [ "$PROVISION_KEY_ID" != null ];
    then
        for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".PROVISION_KEY_ID | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
            value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".PROVISION_KEY_ID[$key]" config-microservicios/QA/Config_Microservicios.json);                    
            realkey=$(echo "${key//\"/}"  | sed  's/\\//')
            sed -i "s|${realkey//\"/}|""${value}""|g" .env.production
        done
fi
CONSUMER=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".CONSUMER" config-microservicios/QA/Config_Microservicios.json)
if [ "$CONSUMER" != null ];
    then
        for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".CONSUMER | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
            value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".CONSUMER[$key]" config-microservicios/QA/Config_Microservicios.json);                    
            realkey=$(echo "${key//\"/}"  | sed  's/\\//')
            sed -i "s|${realkey//\"/}|""${value}""|g" .env.production
        done
fi

if [ -f ".env.production" ];
    then
        variableName="bamboo_SECRET_${config}"
        echo $variableName
        remplazo=${!variableName}
        if [ -z ${!variableName+x} ];
            then
                echo "variable ${variableName} no existe en bamboo. Deteniendo pipeline"; 
                exit 1;
        fi
        decode ${remplazo} > .env.production
    else
        echo "No hay archivo de env.production"
fi
#https://apigwkong-dev.desa.org.mx:8443/wanashop/
#VUE_APP_KONG
#sed -i "s|https://apigwkong-dev.frd.org.mx:8443/wanashop/|"https://apigwkong-dev.desa.org.mx:8443/wanashop/"|g" .env.production
#echo "env prueba"
#cat .env.prueba
echo "env prod"
cat .env.production