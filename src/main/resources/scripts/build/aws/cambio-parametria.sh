#!/bin/bash
set -e
echo 'Obteniendo prefijo y profile'
prefijo=''
profile=''
imagen=''
version=''
if [ -f "config-microservicios/QA/Config_Microservicios.json" ];
    then
        #$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".conexion[$key]" config-microservicios/QA/Config_Microservicios.json);
        prefijo=$(jq ".${bamboo.ambiente}.${bamboo.variante}.stage" config-microservicios/QA/Config_Microservicios.json)
        profile=$(jq ".${bamboo.ambiente}.${bamboo.variante}.profile" config-microservicios/QA/Config_Microservicios.json)
        echo $prefijo
        echo $profile
    else
        echo "No hay archivo de config"

fi

deployment_file=$(find . -name '*.yml')
echo $deployment_file
if [ "${deployment_file}" != "" ] && [ -f ${deployment_file} ];
    then
        cp $deployment_file server.yml
        sed -i 's/!/L/g' server.yml
        sed -i 's/::/L/g' server.yml

        #eval $(parse_yaml server.yml)
        service=$(yq r server.yml "service")
        if [[ $service == *"${self:custom.paquete}"* ]]; then
            echo "reemplazar"
            paquete=$(yq r server.yml "custom.paquete")
            #yml.service = yml.service.replace('${self:custom.paquete}',"${paquete}")
            sed -i 's/${self:custom.paquete}/'"${paquete}"'/g' "server.yml"
        fi

        imagen=$(yq r server.yml "service")
        version="1.0.0"
        #rm server.yml

    else
        echo "No existe el YML"
        exit 1
fi

sed -i "/stage:/ s/dev/${prefijo//\"/}/g" $deployment_file
sed -i 's/profile:/#profile:/g' $deployment_file
sed -i '/NODE_TLS_REJECT_UNAUTHORIZED/ s/0/1/g' $deployment_file
for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".configuracion | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
    #echo $key
    value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".configuracion[$key]" config-microservicios/QA/Config_Microservicios.json);
    realkey=$(echo "${key//\"/}"  | sed  's/\\//')
    sed -i "s|${realkey//\"/}|""${value}""|g" "${deployment_file}"
done
cat $deployment_file


			