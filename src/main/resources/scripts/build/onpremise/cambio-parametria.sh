#!/bin/bash
set -e
echo "cambio parametria"
decode () {
  echo "$1" | base64 -d ; echo
}
if [ -f "configuraciones/conexion.json" ];
    then
        #conexion=$(sudo jq '.${bamboo.ambiente}."${bamboo.variante}".conexion' Config_Microservicios.json)
        for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".conexion | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
            #echo $key
            value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".conexion[$key]" config-microservicios/QA/Config_Microservicios.json);
            #echo $value
            variableName="bamboo_SECRET_${value}"
            remplazo=${!variableName}
            if [ -z ${!variableName+x} ];
            then
                echo "variable ${variableName} no existe en bamboo. Deteniendo pipeline"; 
                exit 1;
            fi
            echo $remplazo
            sed -i "s|${key//\"/}|""${remplazo}""|g" "configuraciones/conexion.json"
        done


    else
        echo "No hay archivo de conexion"

fi

#actualizando llaves
echo "actualizando llaves"
for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".llaves | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
    echo $key
    value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".llaves[$key]" config-microservicios/QA/Config_Microservicios.json);
    #echo $value
	if [ -f "configuraciones/${key//\"/}" ];
            then
                echo "existe configuraciones/${key//\"/}"
                echo "Reemplazando llave"
                variableName="bamboo_SECRET_${value}"
                echo $variableName
                remplazo=${!variableName}
                if [ -z ${!variableName+x} ];
                then
                    echo "variable ${variableName} no existe en bamboo. Deteniendo pipeline"; 
                    exit 1;
                fi
                decode ${remplazo} > configuraciones/${key//\"/}
                #echo $(cat configuraciones/${key//\"/})
            else
		echo "No existe: configuraciones/${key//\"/}"
	fi
done

#actualizando archivo config.json
echo "actualizando archivo config.json"
if [ -f "configuraciones/config.json" ];
    then
        #conexion=$(sudo jq '.${bamboo.ambiente}."${bamboo.variante}".conexion' Config_Microservicios.json)
        for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".secret_file | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
            #echo $key
            value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".secret_file[$key]" config-microservicios/QA/Config_Microservicios.json);
            #echo $value
            variableName="bamboo_${value}"
            remplazo=${!variableName}
            if [ -z ${!variableName+x} ];
            then
                echo "variable ${variableName} no existe en bamboo. Deteniendo pipeline"; 
                exit 1;
            fi
            echo $remplazo
            sed -i "s|${key//\"/}|""${remplazo}""|g" "configuraciones/config.json"
        done


    else
        echo "No hay archivo de config.json"

fi

#actualizando archivo microservicios en caso de que exista
echo "actualizando archivo microservicios en caso de que exista"
if [ -f "configuraciones/microservicios.json" ];
    then
        #conexion=$(sudo jq '.${bamboo.ambiente}."${bamboo.variante}".microservicios' Config_Microservicios.json)
        for key in $(jq '.${bamboo.ambiente}."${bamboo.variante}".microservicios | keys | .[]' config-microservicios/QA/Config_Microservicios.json); do
            #echo $key
            value=$(jq -r ".${bamboo.ambiente}.\"${bamboo.variante}\".microservicios[$key]" config-microservicios/QA/Config_Microservicios.json);
            #echo $value
            realkey=$(echo "${key//\"/}"  | sed  's/\\//')
            echo $realkey
            sed -i "s|${realkey//\"/}|""${value}""|g" "configuraciones/microservicios.json"
            #sudo cat configuraciones/microservicios.json
        done


    else
        echo "No hay archivo de microservicios"

fi
