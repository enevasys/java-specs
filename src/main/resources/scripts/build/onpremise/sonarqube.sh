#!/bin/bash
set -e
whoami
source /etc/profile.d/sonar-scanner.sh


echo 'Obteniendo variables para correr pruebas de sonar'
echo 'probando buscador de nombre'
deployment_file=$(ls YML/*deployment.yml | head -n 1)
echo $deployment_file
imagen=""
tag_image=""
namespace=""
if [ "${deployment_file}" != "" ] && [ -f ${deployment_file} ];
    then
        spec_template_spec_containers_image=$(yq r $deployment_file "spec.template.spec.containers[0].image")
        echo "texto: ${spec_template_spec_containers_image}"
        tag_image=${spec_template_spec_containers_image##*:}
        imagen=$(yq r $deployment_file "metadata.labels.app")
        namespace=$(yq r $deployment_file "metadata.namespace")

    else
        echo "No, no existen los YML"
        exit 1
fi


echo "variables"
version=$tag_image
echo "imagen: $imagen"
echo "namespace: $namespace"
echo "version: $version"

###donde: 
#sonar-scanner scan  -Dsonar.host.url=https://frdqro3304.donde.org.mx/sonar -Dsonar.projectKey=1_${imagen} -Dsonar.projectName=${imagen} -Dsonar.projectVersion=${version} -Dsonar.login=37019edfcb8e98da8aa54c71dc52fa74480d8b9f  -Dsonar.sources=. -Dsonar.language=js -Dsonar.exclusions=test/**,YML/**,mochawesome-report/**,*-report/**,node_modules/** -Dsonar.host.url=https://frdqro3304.donde.org.mx/sonar/ -Dsonar.projectBaseDir=${env.WORKSPACE}

sonar-scanner scan  -Dsonar.host.url=${bamboo.sonarqube} -Dsonar.projectKey=${imagen} -Dsonar.projectName=${imagen} -Dsonar.projectVersion=${version} -Dsonar.login=37019edfcb8e98da8aa54c71dc52fa74480d8b9f  -Dsonar.sources=. -Dsonar.language=js -Dsonar.exclusions=test/**,YML/**,mochawesome-report/**,*-report/**,node_modules/**

echo "Obteniendo resultados del analisis"
#Esperando resultados del analisis.
sleep 10s


curl -k  -u ${bamboo.user_sonar}:${bamboo.password_sonar} ${bamboo.sonarqube}api/qualitygates/project_status?projectKey=${imagen} -o status-sonar.json
cat status-sonar.json
statusSonar=$(jq '.projectStatus.status' status-sonar.json)
echo ${statusSonar}
sonarPass='"OK"'
echo ${sonarPass}
if [ ${statusSonar} = ${sonarPass} ];
then
    echo "Status de sonar OK seguir con pipeline";
    exit 0;
else
    echo "Status de sonar diferente de  OK detener pipeline";
    exit 1;
fi