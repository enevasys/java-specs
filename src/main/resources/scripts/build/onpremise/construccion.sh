#!/bin/bash
set -e
dir
echo 'Obteniendo prefijo'
prefijo=''
prefijo=$(jq '.${bamboo.ambiente}.stage' config-microservicios/QA/Config_Microservicios.json)
echo $prefijo

registry=$(jq '.${bamboo.ambiente}.registry' config-microservicios/QA/Config_Microservicios.json)
echo "registry:"
echo $registry

echo 'Creando directorio imagen'
if [ -d YML/Docker/imagen ];
then
	rm -R YML/Docker/imagen
fi

mkdir YML/Docker/imagen


rsync -Rr * YML/Docker/imagen
rm -rf YML/docker/imagen/YML*

if [ -f YML/Docker/imagen/README.md ];
then
	rm -f YML/Docker/imagen/README.md
fi
					

echo 'Obteniendo variables para guardar la imagen'

deployment_file=$(ls YML/*deployment.yml | head -n 1)
echo $deployment_file

nombre_contenedor=""
tag_image=""
if [ "${deployment_file}" != "" ] && [ -f ${deployment_file} ];
    then
        spec_template_spec_containers_image=$(yq r $deployment_file "spec.template.spec.containers[0].image")
        tag_image=${spec_template_spec_containers_image##*:}
        nombre_contenedor=$(yq r $deployment_file "metadata.labels.app")
    else
        echo "No, no existen los YML"
        exit 1
fi



echo "variables"
echo $nombre_contenedor
echo $tag_image

cd YML/Docker
dir
echo ${nombre_contenedor}

sed -i 's/USER node/#USER node/g' Dockerfile

#donde
docker build -t ${nombre_contenedor}:${tag_image} .
#docker build --rm --no-cache -t ${nombre_contenedor}-enevasys:${tagImage} .

echo 'Tag de Imagen'
echo ${nombre_contenedor,,}
docker tag ${nombre_contenedor}:${tag_image} ${registry//\"/}/${prefijo//\"/}/${nombre_contenedor}:${tag_image}

echo 'Push de Imagen'
echo "docker push ${registry//\"/}/${prefijo//\"/}/${nombre_contenedor}:${tag_image}"
docker push ${registry//\"/}/${prefijo//\"/}/${nombre_contenedor}:${tag_image}

cd ../..