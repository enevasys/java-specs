/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company;

import com.atlassian.bamboo.specs.api.builders.Variable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mikep
 */
public final class Utils {

    public static String readFile(String path, ClassLoader classLoader) {

        File file = new File(classLoader.getResource(path).getFile());
        StringBuilder textBuilder = new StringBuilder();
        try {
            InputStream inputStream = new FileInputStream(file);
            try (Reader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
                int c = 0;
                while ((c = reader.read()) != -1) {
                    textBuilder.append((char) c);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ioe) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ioe);
        }
        return textBuilder.toString();
    }

    public static String readFile(String path) {

        File file = new File(path);
        StringBuilder textBuilder = new StringBuilder();
        try {
            InputStream inputStream = new FileInputStream(file);
            try (Reader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
                int c = 0;
                while ((c = reader.read()) != -1) {
                    textBuilder.append((char) c);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ioe) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ioe);
        }
        return textBuilder.toString();
    }

    public static File[] getResourceFolderFiles(String folder, ClassLoader classLoader) {
        URL url = classLoader.getResource(folder);
        System.out.println(folder);
        File[] files;
        try {
            String path = url.getPath();
            files = new File(path).listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".json");
                }
            });

        } catch (Exception e) {
            return new File[0];
        }
        return files;
    }

    public static Variable[] VariablesToBambooVariable(com.my.company.entities.Variable[] variables) {
        Variable[] result = new Variable[variables.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = new Variable(variables[i].getKey(), variables[i].getValue());
        }
        return result;
    }

    public String getProperty(String property) throws IOException {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            inputStream.close();
            return prop.getProperty(property, "");
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            inputStream.close();

        } finally {
            inputStream.close();
        }
        return "";
    }

    public HashMap<String, String> getPropValues() throws IOException {
        HashMap<String, String> propertiesValues = new HashMap<String, String>();
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            prop.stringPropertyNames().forEach((key) -> {
                String value = prop.getProperty(key);
                propertiesValues.put(key, value);
            });
            inputStream.close();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            inputStream.close();

        } finally {
            inputStream.close();
        }
        return propertiesValues;
    }

    public static String getVariableValue(com.my.company.entities.Variable[] variables, String key) {
        String value = "none";
        for (com.my.company.entities.Variable variable : variables) {
            if (variable.getKey().equals(key)) {
                value = variable.getValue();
            }
        }
        return value;
    }
}
