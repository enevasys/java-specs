package com.my.company;

import com.my.company.templates.build.TemplateUno;
import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.my.company.entities.Plan;
import com.my.company.entities.Project;
import com.my.company.templates.build.TemplateBaseBuild;
import com.my.company.templates.deploy.TemplateBaseDeploy;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Plan configuration for Bamboo. Learn more on:
 * <a href="https://confluence.atlassian.com/display/BAMBOO/Bamboo+Specs">https://confluence.atlassian.com/display/BAMBOO/Bamboo+Specs</a>
 */
@BambooSpec
public class PlanSpec {

    /**
     * Run main to publish plan on Bamboo
     */
    public static void main(final String[] args) throws Exception {
        //By default credentials are read from the '.credentials' file.
        System.out.println(new Utils().getProperty("bambooServer"));
        BambooServer bambooServer = new BambooServer(new Utils().getProperty("bambooServer"));

        //Se manda de parametro el nombre del folder dentro de la carpeta resources :o
        ArchivoJsonConfiguracion projects = new ArchivoJsonConfiguracion("archivos-json-build-deploy");
        List<Project[]> archivos = projects.getProjects();
        
        
        if (archivos != null && archivos.size() >= 1) {
            List<com.atlassian.bamboo.specs.api.builders.plan.Plan> bambooBuildPlans = new ArrayList<>();
            List<Deployment> bambooDeployObjects = new ArrayList<>();
            //recorriendo los diferentes archivos
            for (Project[] proyectos : archivos) {
                //recorriendo los proyectos declarados en cada archivo
                for (Project p : proyectos) {
                    //recorriendo los planes por proyecto
                    for (Plan plan : p.getPlans()) {
                        //build
                        Class<?> buildClass = Class.forName("com.my.company.templates.build." + plan.getTemplateBuild());
                        TemplateBaseBuild template = (TemplateBaseBuild) buildClass.getDeclaredConstructor(TemplateBaseBuild.getConstructorClassArgs()).newInstance(p.getKey(), p.getName(), p.getDescription(), plan.getKey(), plan.getName(), plan.getDescription(), plan.getRepositoryName(), Utils.VariablesToBambooVariable(plan.getVariables()), plan.getBranch(), plan.getRepositoryProjectId());
                        bambooBuildPlans.add(template.getPlan());

                        //deploy
                        String ambiente = Utils.getVariableValue(plan.getVariables(), "ambiente");
                        Class<?> deployClass = Class.forName("com.my.company.templates.deploy." + plan.getTemplateDeploy());
                        TemplateBaseDeploy deploy = (TemplateBaseDeploy) deployClass.getDeclaredConstructor(TemplateBaseDeploy.getConstructorClassArgs()).newInstance(p.getKey(), plan.getKey(), plan.getName(), plan.getDescription(), Utils.VariablesToBambooVariable(plan.getVariables()), ambiente);
                        bambooDeployObjects.add(deploy.createDeploymentObject());
                    }
                }
            }

            
            System.out.println("publicando: " + bambooBuildPlans.size() + " planes");
            bambooBuildPlans.forEach(bambooServer::publish);
            System.out.println("publicando: " + bambooDeployObjects.size() + " deployment plans");
            bambooDeployObjects.forEach(bambooServer::publish);

        }
    }

}
