/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company.entities;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author mikep
 */
public class ConfigurationFile {

    @SerializedName("project")
    private Project[] projects;

    public Project[] getProjects() {
        return projects;
    }

    public void setProjects(Project[] projects) {
        this.projects = projects;
    }

    @Override
    public String toString() {
        return "[projects = " + projects + "]";
    }
}
