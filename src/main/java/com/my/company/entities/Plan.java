/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company.entities;

/**
 *
 * @author mikep
 */
public class Plan {

    private Variable[] variables;

    private String templateDeploy;

    private String name;

    private String branch;

    private String templateBuild;

    private String description;

    private String repositoryName;

    private String repositoryProjectId;

    private String key;

    public Variable[] getVariables() {
        return variables;
    }

    public void setVariables(Variable[] variables) {
        this.variables = variables;
    }

    public String getTemplateDeploy() {
        return templateDeploy;
    }

    public void setTemplateDeploy(String templateDeploy) {
        this.templateDeploy = templateDeploy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplateBuild() {
        return templateBuild;
    }

    public void setTemplateBuild(String templateBuild) {
        this.templateBuild = templateBuild;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranches(String branch) {
        this.branch = branch;
    }

    public String getRepositoryProjectId() {
        return repositoryProjectId;
    }

    public void setRepositoryProjectId(String repositoryProjectId) {
        this.repositoryProjectId = repositoryProjectId;
    }
    
    

    @Override
    public String toString() {
        return "ClassPojo [variables = " + variables + ", templateDeploy = " + templateDeploy + ", name = " + name + ", templateBuild = " + templateBuild + ", description = " + description + ", repositoryName = " + repositoryName + ", key = " + key + "]";
    }
}
