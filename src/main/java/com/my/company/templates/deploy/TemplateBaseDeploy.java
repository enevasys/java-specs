/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company.templates.deploy;

import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.deployment.ReleaseNaming;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.builders.trigger.AfterSuccessfulBuildPlanTrigger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mikep
 */
public abstract class TemplateBaseDeploy {

    private final String projectKey;  //ID único del proyecto
    private final String planKey;  //ID único del proyecto
    private final String deploymentName;
    private final String description;
    protected final Variable[] variables;
    private final String ambiente;

    public TemplateBaseDeploy(String projectKey, String planKey, String deploymentName, String description, Variable[] variables, String ambiente) {
        this.projectKey = projectKey;
        this.planKey = planKey;
        this.deploymentName = deploymentName;
        this.description = description;
        this.variables = variables;
        this.ambiente = ambiente;
    }

    public Deployment createDeploymentObject() {
        List<Environment> environments = getEnvironments();
        final Deployment rootObject = new Deployment(new PlanIdentifier(this.projectKey, this.planKey), deploymentName)
                .description(description)
                .releaseNaming(new ReleaseNaming("release-1")
                        .autoIncrement(true))
                .environments(environments.toArray(new Environment[environments.size()]));
        return rootObject;

    }

    private List<Environment> getEnvironments() {
        List<Environment> environments = new ArrayList<>();
        //Creando el trigger para desarrollo. Si no es desarrollo no se agrega
        String triggerDescription = "Trigger después de un build exitoso";
        AfterSuccessfulBuildPlanTrigger trigger = new AfterSuccessfulBuildPlanTrigger().description(triggerDescription);

        //Environment ambiente = (this.ambiente.equals("desarrollo") || this.ambiente.equals("pruebas")) ? ambiente().triggers(trigger) : ambiente();
        environments.add(ambiente());

//        environments.add(desarrollo().triggers(trigger));
//        environments.add(pruebas().triggers(trigger));
//        environments.add(produccion());

        return environments;
    }

    public abstract Environment ambiente();

    /* Se comentan lineas de los otros ambientes hasta nuevo aviso
    
    public abstract Environment desarrollo();

    public abstract Environment pruebas();

    public abstract Environment produccion();
    */
    public static Class[] getConstructorClassArgs() {
        Class[] cArg = new Class[6]; //Our constructor has 9 arguments
        cArg[0] = String.class;
        cArg[1] = String.class;
        cArg[2] = String.class;
        cArg[3] = String.class;
        cArg[4] = Variable[].class;
        cArg[5] = String.class;

        return cArg;
    }

    public String getAmbiente() {
        return ambiente;
    }

}
