/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company.templates.deploy;

import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.builders.task.ArtifactDownloaderTask;
import com.atlassian.bamboo.specs.builders.task.CleanWorkingDirectoryTask;
import com.atlassian.bamboo.specs.builders.task.DownloadItem;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.my.company.Utils;

/**
 *
 * @author mikep
 */
public class TemplateAws extends TemplateBaseDeploy {

    public TemplateAws(String projectKey, String planKey, String deploymentName, String description, Variable[] variables, String ambiente) {
        super(projectKey, planKey, deploymentName, description, variables, ambiente);
    }

    private ArtifactDownloaderTask obtenerArtefacto() {
        return new ArtifactDownloaderTask()
                .description("Obtener Artefacto")
                .artifacts(new DownloadItem()
                        .artifact("release ZIP"));
    }

    ScriptTask unzipArtifact() {
        final String script = Utils.readFile("scripts/deploy/onpremise/unzip.sh", getClass().getClassLoader());
        //System.out.println(script);
        return new ScriptTask()
                .description("Descomprimir aplicacion")
                .inlineBody(script);
    }

    ScriptTask desplegarAplicacion() {
        final String script = Utils.readFile("scripts/deploy/aws/despliegue.sh", getClass().getClassLoader());
        //System.out.println(script);
        return new ScriptTask()
                .description("Desplegar aplicacion")
                .inlineBody(script);
    }

    @Override
    public Environment ambiente() {
        return new Environment(super.getAmbiente())
                .description(super.getAmbiente())
                .tasks(new CleanWorkingDirectoryTask(), obtenerArtefacto(), unzipArtifact(), desplegarAplicacion()).variables(this.variables);
    }

    /*

    @Override
    public Environment desarrollo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Environment pruebas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Environment produccion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

     */
}
