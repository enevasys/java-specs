/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company.templates.build;

import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.CleanWorkingDirectoryTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.my.company.Utils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mikep
 */
public class TemplateOnPremise extends TemplateBaseBuild {

    public TemplateOnPremise(String projectKey, String projectName, String projectDescription, String planKey, String planName, String planDescription, String repositoryName, Variable[] variables, String branch, String repositoryProjectId) {
        super(projectKey, projectName, projectDescription, planKey, planName, planDescription, repositoryName, variables, branch, repositoryProjectId);
    }

    Stage build() {
        return new Stage("Build")
                .jobs(new Job("Pruebas, cambio de parametria y generar imagen", "RUN").artifacts(new Artifact()
                        .name("release ZIP")
                        .copyPattern("release.zip")
                        .shared(true)
                        .required(true))
                        .tasks(new CleanWorkingDirectoryTask(), checkOut(), pruebasSonarQube(), checkOutConfigMicroservicios(), cambioParametria(), generarZip()));
    }

    VcsCheckoutTask checkOut() {
        return new VcsCheckoutTask()
                .description("Checkout Default Repository")
                .checkoutItems(new CheckoutItem()
                        .repository(new VcsRepositoryIdentifier()
                                .name(this.getRepositoryName())));
    }

    VcsCheckoutTask checkOutConfigMicroservicios() {
        return new VcsCheckoutTask()
                .description("Checkout Config_Microservicios")
                .checkoutItems(new CheckoutItem()
                        .repository(new VcsRepositoryIdentifier()
                                .name(this.getRepositoryPlantillasDespliegue()))
                        .path("config-microservicios"));
    }

    ScriptTask generarZip() {
        final String script = Utils.readFile("scripts/build/onpremise/generar-zip.sh", getClass().getClassLoader());
        //System.out.println(script);
        return new ScriptTask()
                .description("Generar ZIP")
                .inlineBody(script);
    }

    ScriptTask pruebasSonarQube() {
        final String script = Utils.readFile("scripts/build/onpremise/sonarqube.sh", getClass().getClassLoader());
        //System.out.println(script);
        return new ScriptTask()
                .description("Analisis con SonarQube")
                .inlineBody(script);
    }

    ScriptTask cambioParametria() {
        final String script = Utils.readFile("scripts/build/onpremise/cambio-parametria.sh", getClass().getClassLoader());
        //System.out.println(script);
        return new ScriptTask()
                .description("Cambio de parametria en json")
                .inlineBody(script);
    }

    @Override
    public List<Stage> pipeline() {
        List<Stage> stages = new ArrayList<>();
        stages.add(build());
        return stages;
    }

}
