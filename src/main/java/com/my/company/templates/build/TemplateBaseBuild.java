/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company.templates.build;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.applink.ApplicationLink;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.builders.repository.bitbucket.server.BitbucketServerRepository;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.trigger.BitbucketServerTrigger;
import com.my.company.Utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mikep Esta clase tiene las "bases" de cualquier template: key del
 * proyecto, nombre, etc. La idea es que los demas templates extiendan de esta
 * clase.
 */
public abstract class TemplateBaseBuild {

    private final String projectKey;  //ID único del proyecto
    private final String projectName; //Proyecto en bamboo
    private final String projectDescription; //Descripción del proyecto

    private final String planKey;
    private final String planName;
    private final String planDescription;
    private final String repositoryName;
    private final String branch;
    private final String repositoryProjectId;
    private String repositoryPlantillasDespliegue;
    private Plan plan;
    private HashMap<String, String> properties;

    //private List<Plan> plans;
    public TemplateBaseBuild(String projectKey, String projectName, String projectDescription, String planKey, String planName, String planDescription, String repositoryName, Variable[] variables, String branch, String repositoryProjectId) {
        try {
            this.repositoryPlantillasDespliegue = new Utils().getProperty("repositoryPlantillasDespliegue");
        } catch (IOException ex) {
            this.repositoryPlantillasDespliegue = "";
            Logger.getLogger(TemplateBaseBuild.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.projectKey = projectKey;
        this.projectName = projectName;
        this.projectDescription = projectDescription;
        this.planKey = planKey;
        this.planName = planName;
        this.planDescription = planDescription;
        this.repositoryName = repositoryName;
        this.branch = branch;
        this.repositoryProjectId = repositoryProjectId;
        this.plan = createPlan();

        List<Stage> stageList = pipeline();
        System.out.println("Setteando stages");
        System.out.println(stageList.size());
        this.setStages(stageList.toArray(new Stage[stageList.size()]));
        this.setVariables(variables);

    }

    private Plan createPlan() {
        String applicationLinkName = "";
        try {
            applicationLinkName = new Utils().getProperty("applicationLinkName");
        } catch (IOException ex) {
            Logger.getLogger(TemplateBaseBuild.class.getName()).log(Level.SEVERE, null, ex);
        }
        final Plan plan = new Plan(project(), planName, new BambooKey(planKey))
                .description(planDescription)
                .pluginConfigurations(new ConcurrentBuilds())
                .planRepositories(new BitbucketServerRepository()
                        .name(this.repositoryName)
                        .server(new ApplicationLink().name(applicationLinkName))
                        .projectKey(this.repositoryProjectId)
                        .repositorySlug(this.repositoryName)
                        .branch(this.branch))
                .linkedRepositories(repositoryPlantillasDespliegue)
                //.triggers(new BitbucketServerTrigger()
                //        .enabled(true))
                .triggers(new BitbucketServerTrigger()
                        .triggeringRepositoriesType(RepositoryBasedTrigger.TriggeringRepositoriesType.SELECTED)
                        .selectedTriggeringRepositories(new VcsRepositoryIdentifier()
                                .name(this.repositoryName))
                        .description("Trigger")
                        .enabled(true))
                .planBranchManagement(new PlanBranchManagement()
                        .delete(new BranchCleanup())
                        .notificationForCommitters());
        return plan;
    }

    public abstract List<Stage> pipeline();

    private void setStages(Stage[] stages) {
        Plan plan = this.getPlan();
        plan.stages(stages);
        this.setPlan(plan);
    }

    public void setVariables(Variable[] variables) {
        Plan plan = this.getPlan();
        plan.variables(variables);
        this.setPlan(plan);
    }

    Project project() {
        return new Project()
                .name(projectName)
                //.oid(new BambooOid(""))
                .key(projectKey)
                .description(projectDescription);
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public String getRepositoryPlantillasDespliegue() {
        return repositoryPlantillasDespliegue;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public static Class[] getConstructorClassArgs() {
        Class[] cArg = new Class[10]; //Our constructor has 9 arguments
        cArg[0] = String.class;
        cArg[1] = String.class;
        cArg[2] = String.class;
        cArg[3] = String.class;
        cArg[4] = String.class;
        cArg[5] = String.class;
        cArg[6] = String.class;
        cArg[7] = Variable[].class;
        cArg[8] = String.class;
        cArg[9] = String.class;

        return cArg;
    }

}
