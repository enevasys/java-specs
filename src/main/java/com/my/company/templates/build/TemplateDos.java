/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company.templates.build;

import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.my.company.Utils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mikep
 */
public class TemplateDos extends TemplateBaseBuild {

    /**
     *
     * @param projectKey
     * @param projectName
     * @param projectDescription
     * @param planKey
     * @param planName
     * @param planDescription
     * @param deploymentName
     * @param repositoryName
     * @param variables
     */
    public TemplateDos(String projectKey, String projectName, String projectDescription, String planKey, String planName, String planDescription, String repositoryName, Variable[] variables, String branch, String repositoryProjectId) {
        super(projectKey, projectName, projectDescription, planKey, planName, planDescription, repositoryName, variables, branch, repositoryProjectId);
    }

    Stage pruebaStage() {
        final String script = Utils.readFile("template2.sh", getClass().getClassLoader());
        //System.out.println(script);
        return new Stage("Stage 1")
                .jobs(new Job("Build-and-run", "RUN")
                        .tasks(
                                new ScriptTask().inlineBody(script)));
    }

    @Override
    public List<Stage> pipeline() {
        List<Stage> stages = new ArrayList<>();
        stages.add(pruebaStage());
        return stages;
    }
}
