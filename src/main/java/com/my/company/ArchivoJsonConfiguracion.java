/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.company;

import com.google.gson.Gson;
import com.my.company.entities.ConfigurationFile;
import com.my.company.entities.Project;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mikep
 */
public class ArchivoJsonConfiguracion {

    private List<Project[]> projects;
    private String path;

    public ArchivoJsonConfiguracion(String path) {
        this.path = path;
        this.setProjects(obtainProjectsFromFolder());
    }

    private List<Project[]> obtainProjectsFromFolder() {
        List<Project[]> archivosConfiguracion = new ArrayList<>();
        File[] archivos = Utils.getResourceFolderFiles(this.path, getClass().getClassLoader());
        for (File f : archivos) {
            System.out.println(f.toString());
            Gson gson = new Gson();
            String json = Utils.readFile(f.getPath());
            ConfigurationFile file = gson.fromJson(json, ConfigurationFile.class);
            archivosConfiguracion.add(file.getProjects());
        }
        System.out.println("Numero de archivos a analizar: " + archivosConfiguracion.size());
        for (Project[] proyectos : archivosConfiguracion) {
            System.out.println("Proyectos en archivo: " + proyectos.length);
        }

        return archivosConfiguracion;
    }
//    private Project[] obtainProjects(){
//        //File folder = new File(getClass().getClassLoader().getResource("archivos-json-build-deploy").getFile());
//        Gson gson = new Gson();
//        // read your json file into an array
//        System.out.println(this.json);
//        ConfigurationFile file = gson.fromJson(this.json, ConfigurationFile.class);
//        return file.getProjects();
//    }

    public List<Project[]> getProjects() {
        return projects;
    }

    public void setProjects(List<Project[]> projects) {
        this.projects = projects;
    }

    @Override
    public String toString() {
        return "[projects = " + projects + "]";
    }

    public String getPath() {
        return path;
    }

}
